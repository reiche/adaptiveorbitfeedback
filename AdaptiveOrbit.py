import sys
import re
import numpy as np
import copy
import random

from PyQt5 import QtWidgets,QtGui
from PyQt5.uic import loadUiType
from PyQt5 import QtCore


from matplotlib.figure import Figure
import matplotlib.pyplot as plt

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)



import AcquisitionCore



Ui_AdaptiveGUI, QMainWindow = loadUiType('Adaptive.ui')




class Adaptive(QMainWindow, Ui_AdaptiveGUI):
    def __init__(self):
        super(Adaptive, self).__init__()
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon("rsc/icontraj.png"))
        self.setWindowTitle("Adaptive Orbit Correction")

        self.initmpl()
        self.version='3.0.1'

        self.acqAR=AcquisitionCore.Acquisition(True)
        self.acqAT=AcquisitionCore.Acquisition(False)
        self.acq=self.acqAR
        
        self.UIStart.clicked.connect(self.start)
        self.UIStop.clicked.connect(self.stop)
        self.UIFlush.clicked.connect(self.flush)
        self.UICorrectApply.clicked.connect(self.apply)
        self.UICorrectRevert.clicked.connect(self.revert)
        self.UIBeamline.currentIndexChanged.connect(self.changeBL)
        self.UIEnableKicker.stateChanged.connect(self.enableKicker)
        self.UIEnableOrbit.stateChanged.connect(self.enableOrbit)

        self.FB=None
        self.FBback=None
        self.KickerSV = None

        # error sources
        self.errKick=False
        self.errOrbit=False

        self.timer=QtCore.QTimer()
        self.timer.timeout.connect(self.timerEvent)

        quit = QtWidgets.QAction("Quit", self)
        quit.triggered.connect(self.closeEvent)

# basic events

    def changeBL(self):
        self.stop()
        self.flush()
        self.FB=None
        self.FBback=None
        self.UIFBTable.setRowCount(0)
        self.UIEnableKicker.setChecked(False)
        self.UIEnableOrbit.setChecked(False)
        if self.acq.isAramis:
            self.acq=self.acqAT
        else:
            self.acq=self.acqAR

    def closeEvent(self,event):
        self.stop()
        print('Closing window')

    def flush(self):
        self.acqAR.flush()
        self.acqAT.flush()

    def stop(self):
        self.acq.stop()
        self.UIEnableKicker.setChecked(False)
        self.UIEnableOrbit.setChecked(False)
        self.timer.stop()
        self.UIStatus.setText('Stopped')

    def start(self):
        nsam=int(str(self.UISample.text()))
        ntop=int(str(self.UITop.text()))
        self.FB = self.acq.getFB()
        self.FBback=None
        self.updateFBTable(self.FB,self.FBback)
        self.acq.start(nsam,ntop)
        self.initPlot()
        self.timer.start(500)

    def updateFBTable(self,fb1,fb2=None):
        n=len(self.FB)
        self.UIFBTable.setRowCount(n)
        if fb2 is None:
            for i in range(n):
                self.UIFBTable.setItem(i,0,QtWidgets.QTableWidgetItem(self.acq.fbchannels[i].pvname))
                self.UIFBTable.setItem(i,1,QtWidgets.QTableWidgetItem('%8.5f' % fb1[i]))
                self.UIFBTable.setItem(i,2,QtWidgets.QTableWidgetItem('---'))
        else:
            for i in range(n):
                self.UIFBTable.setItem(i,0,QtWidgets.QTableWidgetItem(self.acq.fbchannels[i].pvname))
                self.UIFBTable.setItem(i,1,QtWidgets.QTableWidgetItem('%8.5f' % fb1[i]))
                self.UIFBTable.setItem(i,2,QtWidgets.QTableWidgetItem('%8.5f' % fb2[i]))
        header = self.UIFBTable.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
 

    def getBestOrbit(self):
        xref=0
        yref=0
        wei=0
        for i in range(self.acq.ntop):
            xref+=self.acq.orbx[i,:]*self.acq.top[i]
            yref+=self.acq.orby[i,:]*self.acq.top[i]
        wei=np.sum(self.acq.top)
        if wei <= 0:
            return xref,yref
        return xref/wei,yref/wei




    def timerEvent(self):
        self.UIStatus.setText(self.acq.status)   # since this is parallel only one status is sufficient
        self.UIRate.setText('%d Hz' % int(self.acq.sampleRate))
        if self.errKick:
            self.applyKick()
        elif self.errOrbit:
            self.applyOrbit()
            
        self.plot()  # plotting the Armais statistics
        self.canvas.draw() 


    def initPlot(self):
        self.axes1.clear()
        self.a1lines=[]
        line, = self.axes1.plot(self.acq.zpos,self.FB[0::2],'b-',linewidth=1)
        self.a1lines.append(line)
        line, = self.axes1.plot(self.acq.zpos,self.FB[1::2],'r-',linewidth=1)
        self.a1lines.append(line)
        line, = self.axes1.plot(self.acq.zpos,self.FB[0::2],'c--',linewidth=1)
        self.a1lines.append(line)
        line, = self.axes1.plot(self.acq.zpos,self.FB[1::2],'y--',linewidth=1)
        self.a1lines.append(line)
        for i in range(self.acq.ntop):
            line,=self.axes1.plot(self.acq.zpos,self.acq.orbx[i,:]-self.FB[0::2],'b-',linewidth=0.2)
            self.a1lines.append(line)
            line,=self.axes1.plot(self.acq.zpos,self.acq.orby[i,:]-self.FB[1::2],'r-',linewidth=0.2)
            self.a1lines.append(line)
        self.a1min=-0.002
        self.a1max=0.002
        self.axes1.set_ylim([self.a1min,self.a1max])
        self.axes1.set_xlabel('z (m)')
        self.axes1.set_ylabel('Orbit Deviation (mm)')

        self.axes2.clear()
        self.a2line,=self.axes2.plot(self.acq.hist)
        self.a2max=1
        self.axes2.set_ylim([0,self.a2max])
        self.axes2.set_ylabel(self.acq.channels[1])
        self.axes2.set_xlabel('#Shots')
        if self.acq.isAramis:
            self.axes1.set_title('Aramis: Best and Average Orbits')
            self.axes2.set_title('Aramis: Time Plot of Pulse Energies')
        else:
            self.axes1.set_title('Athos: Best and Average Orbits')
            self.axes2.set_title('Athos: Time Plot of Pulse Energies')
        self.canvas.draw()

    def plot(self):
        if not self.acq.isRunning: 
            return
        self.xref,self.yref=self.getBestOrbit()
        dmin=10
        dmax=-10
        self.a1lines[0].set_ydata(self.xref-self.FB[0::2])
        self.a1lines[1].set_ydata(self.yref-self.FB[1::2])
        self.a1lines[2].set_ydata(self.acq.orbxavg-self.FB[0::2])
        self.a1lines[3].set_ydata(self.acq.orbyavg-self.FB[1::2])
        for i in range(self.acq.ntop):
            dat=self.acq.orbx[i,:]-self.FB[0::2]
            if np.max(dat)>dmax:
                dmax=np.max(dat)
            if np.min(dat)<dmin:
                dmin=np.min(dat)
            self.a1lines[4+2*i].set_ydata(dat)
            dat=self.acq.orby[i,:]-self.FB[1::2]
            if np.max(dat)>dmax:
                dmax=np.max(dat)
            if np.min(dat)<dmin:
                dmin=np.min(dat)
            self.a1lines[5+2*i].set_ydata(dat)

        d=dmax-dmin
        d0=self.a1max-self.a1min
        if d>d0 or d<0.5*d0:
            self.a1max=dmax
            self.a1min=dmin
            self.axes1.set_ylim([self.a1min-0.05*d,self.a1max+0.05*d])

        self.a2line.set_ydata(self.acq.hist)
        tmax=np.max(self.acq.top)
        if tmax > self.a2max:
            self.a2max=1.5*tmax
            self.axes2.set_ylim([0,self.a2max])

    def apply(self):
        self.FBback=[i for i in self.FB]
        self.FB[0::2]=self.xref
        self.FB[1::2]=self.yref
        self.updateFBTable(self.FB,self.FBback)
        self.acq.setFB(self.FB)

    def revert(self):
        self.FB=self.FBback
        self.FBback=None
        self.updateFBTable(self.FB,self.FBback)
        self.acq.setFB(self.FB)

        #set feedback



#-------------------------------
# functions to add noise
    def enableKicker(self):
        self.errKick=self.UIEnableKicker.isChecked()
        if self.errKick:
            rate=float(str(self.UIKickerTime.text()))*1e3/500.   # 500 is timer rate in millisecond
            self.nrate=int(round(rate))
            if self.nrate == 0:
                self.nrate=1
            self.irate=self.nrate
            self.nkick = int(str(self.UIKickerActive.text()))
            self.ikick = self.nkick
            self.nkickrest=int(str(self.UIKickerPaused.text()))
            if self.acq.isAramis:
                pv1=str(self.UIKickerAramis1.text())
                pv2=str(self.UIKickerAramis2.text())
                self.pvFB = 'SFB_ORBIT_SAR:ONOFF1'
            else:
                pv1=str(self.UIKickerAthos1.text())
                pv2=str(self.UIKickerAthos2.text())
                self.pvFB = 'SFB_ORBIT_SAT:ONOFF1'

            self.KickerSV=self.acq.getKicker([pv1,pv2])
            self.enableFB=self.acq.getPV(self.pvFB)
            self.acq.setPV(self.pvFB,0)
        else:
            if self.KickerSV is None:
                return
            self.acq.setKicker(self.KickerSV)
            self.acq.setPV(self.pvFB,self.enableFB)
            print('Stopping Kicker Noise')


    def applyKick(self):
        self.irate -= 1
        if self.irate > 0:
            return
        self.irate=self.nrate
        self.ikick -= 1
        if self.ikick > 0:
            print('kicker noise')
            dmax=float(str(self.UIKickerVar.text()))
            delKickerSV=self.KickerSV*0
            for i in range(len(self.KickerSV)):
                delKickerSV[i]=random.uniform(-dmax,dmax)
            print(delKickerSV)
            self.acq.setKicker(self.KickerSV+delKickerSV)
        elif self.ikick == 0:
            print('enabling FB')
            self.acq.setPV(self.pvFB,self.enableFB)
        elif self.ikick < -self.nkickrest:
                print('restarting kicker')
                print('disabling FB')
                self.ikick=self.nkick
                self.acq.setPV(self.pvFB,0)
        else:
            print('kicker paused')


    def enableOrbit(self):
        self.errOrbit=self.UIEnableOrbit.isChecked()
        if self.errOrbit:
            rate=float(str(self.UIOrbitTime.text()))*1e3/500.   # 500 is timer rate in millisecond
            self.nrate=int(round(rate))
            if self.nrate == 0:
                self.nrate=1
            self.irate=self.nrate
        else:
            self.acq.setFB(self.FB)
            print('Stopping Orbit Noise')
        
    def applyOrbit(self):
        self.irate -= 1
        if self.irate > 0:
            return
        self.irate=self.nrate

        print('Applying Noise to the FB ORbit')
        nerr=int(str(self.UIOrbitBPM.text()))
        dmax=float(str(self.UIOrbitVar.text()))*1e-3
        nbpm=int(len(self.FB)/2)
        idx=[i for i in range(nbpm)]
        selx=random.sample(idx,nerr) 
        sely=random.sample(idx,nerr) 
        delFB=self.FB*0
        for i in range(nerr):
            delFB[2*selx[i]]=random.uniform(-dmax,dmax)
            delFB[2*sely[i]+1]=random.uniform(-dmax,dmax)
        self.acq.setFB(self.FB+delFB)


#------------------------
# initializing matplotlib
    def initmpl(self):

        self.fig=Figure()
        self.axes1=self.fig.add_subplot(211)
        self.axes2=self.fig.add_subplot(212)
        self.canvas = FigureCanvas(self.fig)
        self.mplvl.addWidget(self.canvas)
        self.canvas.draw()
        self.toolbar=NavigationToolbar(self.canvas,self.mplwindow, coordinates=True)
        self.mplvl.addWidget(self.toolbar)
 
 
    # --------------------------------
    # Main routine


if __name__ == '__main__':
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("plastique"))
    app = QtWidgets.QApplication(sys.argv)
    main = Adaptive()
    main.show()
    sys.exit(app.exec_())
