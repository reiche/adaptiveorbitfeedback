import sys
import re
import numpy as np
import copy
import random
import time

from PyQt5 import QtWidgets,QtGui
from PyQt5.uic import loadUiType
from PyQt5 import QtCore


from matplotlib.figure import Figure
import matplotlib.pyplot as plt

from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)



import JitterCore






if __name__ == '__main__':
    acq=JitterCore.Acquisition(True)
    acq.start()
    while acq.isRunning:
        time.sleep(1)
    idodd=0
    ideven=0
    orbodd=0
    orbeven=0
    for i,id0 in enumerate(acq.id):
        if (id0 % 2) == 0:
            ideven+=1
            orbeven+=acq.orb[i,:]
        else:
            idodd+=1
            orbodd+=acq.orb[i,:]
    orbeven/=float(ideven)
    orbodd/=float(idodd)
    print(ideven,idodd)
    plt.plot(orbeven[0::2]-orbodd[0::2],label=r'X-Orbit')
    plt.plot(orbeven[1::2]-orbeven[1::2],label=r'Y-Orbit')
    plt.legend()
    plt.xlabel('BPMs')
    plt.ylabel('$\Delta$x,$\Delta$y (mm)')
    plt.title('Mean Orbit (odd IDs) - Mean Orbit (even IDs)')
    plt.show()


    print('done')
