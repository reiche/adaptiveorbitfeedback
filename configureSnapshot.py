from  epics import caget
import json

import sys
sys.path.append('/sf/bd/applications/OnlineModel/current')


from OMFacility import *

tout=1

pvs=[]

SwissFEL=Facility(1,0)   # near future version
elements=SwissFEL.listElement('*',1)
for ele in elements:
    name=ele.Name.replace('.','-')
    # magnets
    if 'MSEX' in name: 
        att=['I-SET','K2L-SET','ENERGY-OP']
        for at in att:
            pv='%s:%s' % (name,at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)

    if 'MBND' in name: 
        att=['I-SET','ANGLE-CALC','K0L-SET','ENERGY-OP']
        for at in att:
            pv='%s:%s' % (name,at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)
        if 'corx' in ele.__dict__.keys():
            name=name[0:7]+'-MCOR'+name[12:15]


    if 'MQUA' in name: 
        att=['I-SET','K1L-SET','ENERGY-OP']
        for at in att:
            pv='%s:%s' % (name,at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)
        if 'corx' in ele.__dict__.keys():
            name=name[0:7]+'-MCOR'+name[12:15]

    if 'MSOL' in name: 
        att=['I-SET','ENERGY-OP']
        for at in att:
            pv='%s:%s' % (name,at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)

    if 'MCOR' in name or 'MCRX' in name or 'MCRY' in name:
        att=['I-SET']
        for at in att:
            pv='%s-MCRX%s:%s' % (name[0:7],name[12:15],at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)
            pv='%s-MCRY%s:%s' % (name[0:7],name[12:15],at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)

# undulator
    if 'SAR' in name and 'UIND' in name:
        att=['GAP_SP','K_SET']
        for at in att:
            pv='%s:%s' % (name,at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)

    if 'UPHS' in name:
        att=['GAP_SP','PHI_SET']
        for at in att:
            pv='%s:%s' % (name,at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)


    if 'SINLH' in name and 'UIND' in name:
        att=['GAP-SP','K-SP','BEAMPIPE-SP','X-SP']
        for at in att:
            pv='%s:%s' % (name,at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)

# RF
    if '-R' in name and '100' in name:
        att=['RMSM:SM-SET','RSYS:REQUIRED-OP','RSYS:SET-ACC-VOLT','RSYS:SET-BEAM-PHASE']
        for at in att:
            pv='%s-%s' % (name[0:7],at)
            val=caget(pv,timeout=tout)
            if val is not None:
                pvs.append(pv)


with open('snapshot.conf','w') as f:
    for ele in pvs:
        f.write('%s\n' % ele)



