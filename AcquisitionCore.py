import numpy as np
import logging
import socket
import datetime
import random
from threading import Thread

#Qt specific libraries
from PyQt5.QtCore import pyqtSignal,QThread

#PSI libraries for bsread
from bsread import source

from epics import PV


class Acquisition(QThread):
    def __init__(self,Aramis=True):

        QThread.__init__(self) 

        # logging
        logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S')

        self.logger=logging.getLogger('Adaptive Orbit - Acquisition')
        self.logger.info('started at %s' % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.logger.info('Version: 3.0.1 ')
        self.logger.info('Host: %s' % socket.gethostname())
        self.isAramis=Aramis
        self.isRunning=False
        self.doStop=False
        self.status='Stopped'
        self.constructChannels()


    def constructChannels(self):
        if self.isRunning:
            self.logger.info('Channel can only be configured if acquisition is not running')
            return
        #basic channels
        self.zpos=[]
        self.fbchannels=[]
        self.kicker=[]
        if self.isAramis:
            self.channels=['SIN-CVME-TIFGUN-EVR0:BUNCH-1-OK','SARFE10-PBIG050-EVR0:CALCI']
            for i in range(1,17):
                self.channels.append('SARUN%2.2d-DBPM070:X1' % i)
                self.channels.append('SARUN%2.2d-DBPM070:Y1' % i)
                self.fbchannels.append(PV('SARUN%2.2d-DBPM070:X-REF-FB' % i))
                self.fbchannels.append(PV('SARUN%2.2d-DBPM070:Y-REF-FB' % i))
                self.zpos.append((i-1)*4.75)
        else:
#            self.channels=['SIN-CVME-TIFGUN-EVR0:BUNCH-2-OK','SATFE10-PEPG046:FCUP-INTENSITY-CAL']
#            self.channels=['SIN-CVME-TIFGUN-EVR0:BUNCH-2-OK','SATFE10-PEPG046-EVR0:CALCI']
            self.channels=['SIN-CVME-TIFGUN-EVR0:BUNCH-2-OK','SATFE10-PEPG046-EVR0:CALCX']
            bpm=[6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21] # 13 is removed since it triggers  only with 5 Hz
            for i in bpm:
                idx='070'
                if i == 14:
                    idx='410'
                self.channels.append('SATUN%2.2d-DBPM%s:X1' % (i,idx))
                self.channels.append('SATUN%2.2d-DBPM%s:Y1' % (i,idx))
                self.fbchannels.append(PV('SATUN%2.2d-DBPM%s:X-REF-FB' % (i,idx)))
                self.fbchannels.append(PV('SATUN%2.2d-DBPM%s:Y-REF-FB' % (i,idx)))
                self.zpos.append((i-9)*2.8)            
        self.nbpm=int((len(self.channels)-2)/2)
        self.nstat=10000
        self.ntop =10
        self.top  =np.zeros(self.ntop)
        self.hist = np.zeros(self.nstat)
        self.orbx=np.zeros((self.ntop,self.nbpm))
        self.orby=np.zeros((self.ntop,self.nbpm))
        self.orbxavg=np.zeros(self.nbpm)
        self.orbyavg=np.zeros(self.nbpm)
        self.lastId=-1
        self.sampleRate=0

    # get the current fb readings for storage
    def getPV(self,PVchannel):
        pv=PV(PVchannel)
        return pv.get()

    def setPV(self,PVchannel,val):
        pv=PV(PVchannel)
        pv.put(val)

    def getFB(self):
        return np.array([p.get() for p in self.fbchannels])

    # set the new feedback values
    def setFB(self,fbval):
        print('setting fbvalues')
        for i in range(len(fbval)):
            self.fbchannels[i].put(fbval[i])

    def getKicker(self,pvs):
        self.kicker.clear()
        for pv in pvs:
            self.kicker.append(PV('%s:I-SET' % pv.replace('MQUA','MCRX')))
            self.kicker.append(PV('%s:I-SET' % pv.replace('MQUA','MCRY')))
        return np.array([p.get() for p in self.kicker])

    def setKicker(self,val):
        for i,pv in enumerate(self.kicker):
            pv.put(val[i])

    
    def flush(self):
        self.hist*=0
        self.top*=0
        self.orbx*=0
        self.orby*=0
        self.orbxavg*=0
        self.orbyavg*=0
        
    def stop(self):
        self.doStop=True
        self.status='Stopped'

    def start(self,nsam=10000,ntop=10):
        if self.isRunning:
            return
        self.logger.info('Start Thread for Scan')
        self.nstat=nsam
        self.stat =np.zeros(self.nstat)
        self.hist =np.zeros(self.nstat)
        self.ntop =ntop
        self.top  =np.zeros(self.ntop)        
        self.orbx=np.zeros((self.ntop,self.nbpm))
        self.orby=np.zeros((self.ntop,self.nbpm))
        self.lastID=-1
        Thread(target=self.core).start()
        
    def core(self):
        self.isRunning=True
        self.doStop=False
        self.status='Running'
        with source(channels=self.channels) as stream:
            while not self.doStop:
                msg=stream.receive()   # read BS 

                # check for wrong readout
                skip=False
                for chn in self.channels:
                    if msg.data.data[chn].value is None:
                        skip=True
                if skip or msg.data.data[self.channels[0]].value == 0:
                    continue

                if self.lastID<0:
                    self.sampleRate=0
                else:
                    self.sampleRate=100./(msg.data.pulse_id-self.lastID)
                self.lastID=msg.data.pulse_id

                # average orbit with decaying weight
                for i in range(self.nbpm):
                    self.orbxavg[i]=0.97*self.orbxavg[i]+0.03*msg.data.data[self.channels[2+2*i]].value
                    self.orbyavg[i]=0.97*self.orbyavg[i]+0.03*msg.data.data[self.channels[3+2*i]].value
                    
                # selecting top results
                evt=msg.data.data[self.channels[1]].value
                self.hist=np.roll(self.hist,-1)
                self.hist[self.nstat-1]=evt

                idx=np.argmin(self.top)
                if evt > self.top[idx]:
                    self.top[idx]=evt
                    for i in range(self.nbpm):
                        self.orbx[idx,i]=msg.data.data[self.channels[2+2*i]].value
                        self.orby[idx,i]=msg.data.data[self.channels[2+2*i+1]].value
        self.logger.info('Stopping Thread for Scan')
        self.isRunning=False
