import numpy as np
import logging
import socket
import datetime
import random
from threading import Thread

#Qt specific libraries
from PyQt5.QtCore import pyqtSignal,QThread

#PSI libraries for bsread
from bsread import source

from epics import PV


class Acquisition(QThread):
    def __init__(self,Aramis=True):

        QThread.__init__(self) 

        # logging
        logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S')

        self.logger=logging.getLogger('Adaptive Orbit - Acquisition')
        self.logger.info('started at %s' % datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        self.logger.info('Version: 2.0.1 ')
        self.logger.info('Host: %s' % socket.gethostname())
        
        self.channels=['SIN-CVME-TIFGUN-EVR0:BUNCH-1-OK','SARFE10-PBIG050-EVR0:CALCI']
        for i in range(1,20):
            self.channels.append('SARUN%2.2d-DBPM070:X1' % i)
            self.channels.append('SARUN%2.2d-DBPM070:Y1' % i)
        self.channels.append('SARCL02-DBPM110:X1')
        self.channels.append('SARCL02-DBPM110:Y1')

           
        self.nbpm=int((len(self.channels)-2)/2)
        self.nstat=5000
        self.orb=np.zeros((self.nstat,2*self.nbpm))
        self.isRunning=False
        self.id=np.zeros((self.nstat))



    def start(self):
        if self.isRunning:
            return
        Thread(target=self.core).start()
        
    def core(self):
        self.isRunning=True
        self.status='Running'
        icount = 0
        with source(channels=self.channels) as stream:
            while icount < self.nstat:
                msg=stream.receive()   # read BS 
                skip=False
                for chn in self.channels:                    
                    if msg.data.data[chn].value is None:
                        skip=True
                if skip or msg.data.data[self.channels[0]].value == 0:
                    continue


                self.id[icount]=msg.data.pulse_id
                for ibpm in range(self.nbpm):
                    self.orb[icount,2*ibpm]  =msg.data.data[self.channels[2+2*ibpm]].value
                    self.orb[icount,2*ibpm+1]=msg.data.data[self.channels[3+2*ibpm]].value
                icount+=1
        self.logger.info('Stopping Thread for Scan')
        self.isRunning=False
